import { Server } from '@hapi/hapi';

const init = async () => {
    const server: Server = new Server({
        port: 8000,
        host: 'localhost'
    });
    server.route({
        method: 'GET',
        path: '/',
        handler: () => {
            return 'Hello World';
        }
    });
    await server.start();
    console.log(`Server running on ${server.info.uri}`);
};

process.on('unhandledRejection', (error) => {
    console.log(error);
    process.exit(1);
});

init();